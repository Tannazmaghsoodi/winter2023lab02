import java.util.Scanner;
public class PartThree{
  public static void main(String[] args){
    Scanner keyboard = new Scanner(System.in);
    int squareSide = keyboard.nextInt();
    System.out.println(AreaComputations.areaSquare(squareSide));
    int rectangleLength = keyboard.nextInt();
    int rectangleWidth = keyboard.nextInt();
    AreaComputations sc = new AreaComputations();
    System.out.println(sc.areaRectangle(rectangleLength,rectangleWidth));
  }
}
    
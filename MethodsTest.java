public class MethodsTest{
  public static void main(String[] args){
    int x= 5; 
    System.out.println(x);
    methodNoInputNoReturn();
    System.out.println(x);
    methodOneInputNoReturn(10);
    methodOneInputNoReturn(x);
    methodOneInputNoReturn(x+50);
    methodTwoInputNoReturn(5,6.8);
    
   int z = methodNoInputReturnInt();
   System.out.println(z);
   double test8 = sumSquareRoot(6,3);
   System.out.println(test8);
   
   String s1 = "hello";
   String s2 = "goodbye";
   System.out.println(s1.length());
   System.out.println(s2.length());
   
   System.out.println(SecondClass.addOne(50));
   SecondClass sc = new SecondClass();
  System.out.println(sc.addTwo(50));
    
  }
  public static void methodNoInputNoReturn(){
    System.out.println("I'm in a method that takes no input and returns nothing");
    int x= 20; 
    System.out.println(x);
    
  }
  public static void methodOneInputNoReturn(int y){
    System.out.println("Inside this method one input no return");
    System.out.println(y);
    
}
  public static void methodTwoInputNoReturn(int number1, double number2){
    System.out.println(number1 + "," + number2);

  }
  public static int methodNoInputReturnInt(){
    return 6;
  }
  
  public static double sumSquareRoot(int firstNumber, int secondNumber){
    int sum = firstNumber+secondNumber;
    double squareRoot = Math.sqrt(sum);
    return squareRoot;
  }
  
  
}
    
  